<div class="page-header">
    <h1>T005</h1>
    <hr />
    <div class="card">
        <div class="card-body">
            <p class="lead">
                Modificar el código html/javascript para enviar la información de login vía ajax al archivo <code>tests/inc/login.php</code> al apretar el botón de <code>Iniciar sesión</code>
                <br />
                Generar el código en <code>tests/inc/login.php</code> necesario para procesar la información y validar/invalidar inicios de sesión
                <br />
                Enviar usuario y password a <code>tests/inc/login.php</code> y procesar la respuesta
                <br />
                Mostrar mensaje <b>#msgAlertError</b> si los datos de sesión son inválidos
                <br />
                En caso de login exitoso, ocultar <b>#loginBox</b> y mostrar <b>#profileBox</b> con la información del usuario devuelta por el request <b class="text-danger">#userAvatar,#profileName</b>
                <br />
                Las imágenes de los avatares se obtienen de <code>resources/avatars/{{imagen}}</code>
                <br />
                Al apretar en el botón de <code>cerrar sesión</code>, volver a mostrar el formulario <b>#loginBox</b> y ocultar <b>#profileBox</b>
                <br />
                Leer <code>tests/inc/login.php</code> para obtener credenciales y lógica de login
                <br />
            </p>
        </div>
    </div>
    <hr />
</div>
