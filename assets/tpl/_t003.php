<div class="page-header">
    <h1>T003</h1>
    <hr />
    <div class="card">
        <div class="card-body">
            <p class="lead">
                Utilizar dump de mysql en <code>resources/db/testphp.sql</code>
                <br />
                <b>(importar sql a motor de base de datos para poder conectar y consultar)</b>
                <br />
                Generar un calendario para el mes <code>05 de 2022</code>
                <br />Consultar db y extraer información para generar la agenda de eventos
                <br /><code>Utilizar PDO</code>
                <br />Para cada día con evento, imprimir etiqueta con <code>class="event"</code>, asignar <code>título, author, horario</code>, y asignarle la clase correspondiente a envType (<code>zoom, google-meet, office, other</code>)
                <br />Listar solo los eventos activos (<code>enabled = 1</code>) pertenecientes al mes de mayo 2022
                <br />Asignar clase <code>"not-current-month"</code> si el día no pertenece a mayo 2022 al crear la grilla
                <br />Utilizar  <code>ROOT_PATH.'config/config.php'</code> para settear información de conexión a db
            </p>
        </div>
    </div>
    <hr />
</div>
