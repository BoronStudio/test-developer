# Prueba PHP2023
~~~
Clonar el repositorio a la carpeta raíz del un servidor apache/nginx con php 7.x y mysql 5.x
~~~


#### Se debe entregar solo el contenido de la carpeta "test" en un archivo comprimido.

* Utilizar la carpeta test/inc/ para incluir cualquier libreria/código/clase auxiliar.
* En caso de conectar con la base de datos, utilizar el archivo /config/config.php para las credenciales, (no enviar este archivo)
* Cada test es independiente y se puede resolver de varias maneras.
* En caso de no poder resolver algún requisito de forma dinámica, se puede generar helpers estáticos para poder avanzar en la resolución del problema
* Se valorará código prolijo y estructurado, utilizar test/inc/ para generar cualquier estructura necesaria
