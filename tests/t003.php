<?php
if (!defined("INC_FILE_LOADED")) {
    die("...");
}
include(ROOT_PATH . 'assets/tpl/_t003.php');

/**
 * Utilizar dump de mysql en resources/db/testphp.sql (importar sql a motor de base de datos para poder conectar y consultar)
 * Generar un calendario para el mes 05 de 2022 y mostrar los días que contienen eventos
 * Consultar db y extraer información para generar la agenda de eventos
 * Utilizar PDO
 * Para cada día con evento, imprimir etiqueta con class="event" y asignar título, author, horario, y asignarle la clase correspondiente a envType (zoom, google-meet, office, other)
 * Listar solo los eventos activos (enabled = 1) pertenecientes al mes de mayo 2022
 * Asignar clase "not-current-month" si el día no pertenece a mayo 2022
 **/
CONST CALENDAR_YEAR = "2022";
CONST CALENDAR_MONTH = "05";


?>


<div class="card">
    <div class="card-header">Events (May 2022)</div>
    <div class="card-body">
        <div class="calendar" data-toggle="calendar">
            <div class="row rowheader">
                <div class="col-xs-12 calendar-day">
                    <span>MON</span>
                </div>
                <div class="col-xs-12 calendar-day">
                    <span>TUE</span>
                </div>
                <div class="col-xs-12 calendar-day">
                    <span>WED</span>
                </div>
                <div class="col-xs-12 calendar-day">
                    <span>THU</span>
                </div>
                <div class="col-xs-12 calendar-day">
                    <span>FRI</span>
                </div><div class="col-xs-12 calendar-day">
                    <span>SAT</span>
                </div>
                <div class="col-xs-12 calendar-day">
                    <span>SUN</span>
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12 calendar-day not-current-month">
                    <time datetime="2014-06-29">29</time>
                </div>
                <div class="col-xs-12 calendar-day not-current-month">
                    <time datetime="2014-06-30">30</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-01">01</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-02">02</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-03">03</time>
                    <div class="events">
                        <div class="event office">
                            <h4>Title</h4>
                            <h6>author name</h6>
                            <h5>20:30 hrs</h5>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-04">04</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-05">05</time>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-06">06</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-07">07</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-08">08</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-09">09</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-10">10</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-11">11</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-12">12</time>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-13">13</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-14">14</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-15">15</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-16">16</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-17">17</time>
                    >
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-18">18</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-19">19</time>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-20">20</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-21">21</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-22">22</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-23">23</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-24">24</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-25">25</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-26">26</time>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-27">27</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-28">28</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-29">29</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-30">30</time>
                </div>
                <div class="col-xs-12 calendar-day">
                    <time datetime="2014-07-31">31</time>
                </div>
                <div class="col-xs-12 calendar-day not-current-month">
                    <time datetime="2014-08-01">01</time>
                </div>
                <div class="col-xs-12 calendar-day not-current-month">
                    <time datetime="2014-08-02">02</time>
                </div>
            </div>
        </div>
    </div>
</div>