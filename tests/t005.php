<?php
if (!defined("INC_FILE_LOADED")) {
    die("...");
}
include(ROOT_PATH . 'assets/tpl/_t005.php');

/**
 * Modificar estructura HTML y agregar javascript necesario para hacer funcionar el formulario de login
 * de forma asincrónica contra test/inc/login.php
 **/
?>


<div class="card">
    <div class="card-header">Login test</div>
    <div class="card-body">
        <!-- START LOGIN BOX -->
        <div class="container" id="loginBox">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="" method="post">

                            <h3 class="text-center text-dark">Iniciar sesión</h3>
                            <div class="form-group">
                                <label for="username" class="text-primary">Username:</label><br>
                                <input type="text" name="username" id="username" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-primary">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>
                            <hr/>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary btn-md">Iniciar sesión</button>
                            </div>
                            <div class="row mt-5">
                                <div class="alert alert-danger" role="alert" style="display: none;" id="msgAlertError">
                                    Usuario / contraseña inválido
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END LOGIN BOX -->

        <div class="container" id="profileBox" style="display:none;">
            <div class="row profile justify-content-center align-items-center">
                <div class="col-md-6">
                    <div class="profile-sidebar">
                        <div class="form-group profileAvatar text-center">
                            <img src="{{AGREGAR IMAGEN}}" alt="" id="userAvatar">
                        </div>
                        <div class="row profileTitle">
                            <div class="profileName" id="profileName">{{AGREGAR NOMBRE}}</div>
                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-danger btn-sm" id="btnLogout">Logout</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<script>


</script>