<?php
if (!defined("INC_FILE_LOADED")) {
    die("...");
}
include(ROOT_PATH . 'libs/inc/Test04Helper.php');
include(ROOT_PATH . 'assets/tpl/_t004.php');

/**
 * Escribir / modificar el contenido de las funciones para que generen la salida correcta
 **/
?>

<?php
/**
 * Extraer y retornar todos los números (0-9) de un string dado
 * @param string $txtString
 */
function printOnlyNumbersFromString(string $txtString){

}


/**
 * Recibe un array asociativo con una key = 'name'
 * Devolver el mismo array pero ordenado alfabéticamente por la clave name
 * @param array $arrItems
 */
function sortArrayByIndexName(array $arrItems)
{

}

/**
 * La funcion recibe 2 parámetros, fecha desde y fecha hasta, con formato dd/mm/AAAA
 * Devolver un listado con todas las fechas que sean lunes en formato dd/mm/AAAA incluidas en el rango dado
 * @param string $from Dia inicial del rango de fechas (formato dd/mm/YYYY)
 * @param string $to Día final del rango de fechas (formato dd/mm/YYYY)
 * @return array
 */
function getMondaysFromDateRange(string $from, string $to){

}

/**
 * Obtener a partir del log que se recibe como parámetro una lista con ips únicas que consultaron por el archivo robots.txt
 * Analizar archivos en ROOT_PATH.'resources/logs/'
 * @param string $logPath
 * @return array    listado con ip's únicas
 */
function getIpFromCheckRobotsTxtLog(string $logPath){

}

?>

<div class="card">
    <div class="card-header"></div>
    <div class="card-body">
        <div class="row">
            <?php Test04Helper::runFunctionStack(); ?>
        </div>
    </div>
</div>