<?php
if (!defined("INC_FILE_LOADED")) {
    die("...");
}
include(ROOT_PATH . 'assets/tpl/_t002.php');

/**
 * Leer las imágenes del directorio ROOT_PATH.'/resources/images/'
 * Listarlas ordenadas por el indice 01,02,03 etc que contiene en el nombre zzz_01.ext
 * Imprimir las imágenes usando el template HTML listando la información si está disponible (img, nombre archivo, ancho, alto, tipo mime, tamaño, artista/autor)
 **/

?>

<div class="card">
    <div class="card-body">
        <table class="table">
            <thead>
            <tr>
                <th scope="col" style="width:100px">Thumb</th>
                <th scope="col">Name</th>
                <th scope="col">Width</th>
                <th scope="col">Height</th>
                <th scope="col">Type</th>
                <th scope="col">Size</th>
                <th scope="col">Artist/author</th>
            </tr>
            </thead>
            <tbody>
            <!-- START ROW INFO SNIPPET -->
            <tr>
                <th scope="row">
                    <img class="rounded-circle shadow-1-strong me-3" src="resources/images/PM_01.jpg" alt="avatar" width="80" height="80">
                </th>
                <td>PM_01.jpg</td>
                <td>283</td>
                <td>283</td>
                <td>image/jpeg</td>
                <td>92.14 kB</td>
                <td>Roberwave</td>
            </tr>
            <!-- END ROW INFO SNIPPET -->

            </tbody>
        </table>
    </div>
</div>